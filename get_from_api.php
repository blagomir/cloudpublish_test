<?php


include('mysql.php');

/**
 * I would usually use Guzzle or cURL here, but for speed I use file_get_contents.
 */

$APISecret = 't1sv9ooegfsdl4050Sopgt34FA';
$APIUser = 'TEST1';

$URL = 'http://api.cloudpublish.co.uk/?';
$query = 'method=BookGetAll&args=' . urlencode('A') . '&nonce=' . urlencode(uniqid());

$query = 'method=BookGetAll&args=' . urlencode('availability=on_sale&published_after=2019-01-01') . '&p=' . $APIUser . '&nonce=' . urlencode(uniqid());


$hash = md5($query . $APISecret);

$call = $URL . $query . '&h=' . $hash;


$t = file_get_contents($call);

$t = json_decode($t);

if ($t->code == 200) {
    $books = $t->data;


    foreach ($books as $book) {
        if (strpos($book->authors, 'A') === 0) {
            save($book->title, $book->authors, $conn);
            echo 'OK';
        }
    }

} else {
    /**
     * Email someone? Log error?
     */
}


/**
 * Any framework would handle this better and more readable. I don't like the last argument being there.
 * @param $title
 * @param $author
 * @param $conn
 */
function save($title, $author, $conn)
{
    $sql = 'INSERT INTO books SET title="' . htmlspecialchars($title) . '", author="' . htmlspecialchars($author) . '"';
    mysqli_query($conn, $sql);
}

